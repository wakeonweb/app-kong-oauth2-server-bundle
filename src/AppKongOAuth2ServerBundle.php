<?php

namespace App\Bundle\KongOAuth2ServerBundle;

use App\Bundle\KongOAuth2ServerBundle\DependencyInjection\AppKongOAuth2ServerExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * @author Quentin Schuler <q.schuler@wakeonweb.com>
 */
class AppKongOAuth2ServerBundle extends Bundle
{
    /**
     * {@inheritdoc}
     */
    public function getContainerExtension()
    {
        return new AppKongOAuth2ServerExtension();
    }
}
